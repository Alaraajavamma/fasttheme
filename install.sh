#!/bin/bash
gsettings set org.gnome.desktop.background picture-uri "file:///home/$USER/.themes/fasttheme/tuxgnu.png"
mkdir -p ${HOME}/.config/gtk-3.0/

mv ${HOME}/.config/gtk-3.0/gtk.css ${HOME}/.config/gtk-3.0/backupcss

cd ${HOME}/.config/gtk-3.0/
cat << EOF > gtk.css
/* lockscreen background */
phosh-lockscreen, .phosh-lockshield {
  background-image: url('file://$HOME/.themes/fasttheme/tuxgnu.png');
  background-size: cover;
  background-position: top;
}

/* quicksettings background */
phosh-top-panel {
  background-image: url('file://$HOME/.themes/fasttheme/tuxround.png');
  background-size: cover;
  background-position: top;
}

/* squeekboard background */
sq_view {
    background: #00324d;
    color: #ffffff; /* color of the characters in the key */
}

/* squeekboard buttons */
sq_view sq_button {
    background: #c42529;
    color: #ffffff; /* color of the characters in the key */
    border: none;
    border-radius: 5px;

}

/* squeekboard locked buttons - for example press shift twice */
sq_button.locked {
    background: #ffffff;
    color: #c42529; /* color of the characters in the key */
    border: none;
    border-radius: 5px;

}

/* squeekboard latched buttons - for example press shift once */
sq_button.latched {
    background: #c42529;
    border-color: #ffffff;
    border: solid;
    border-width: 1px;
    border-radius: 5px;
}

/* squeekboard pressed button to indicate what did you type */
sq_button:active {
    background: #c42529;
    border-color: #ffffff;
    border: solid;
    border-width: 1px;
    border-radius: 5px;
}

/* squeekboard my just a tweak to my own layout */
sq_button.small2 {
    font-size: 0.3em;
}
sq_button.small {
    font-size: 0.3em;
}

/* Phosh stuff below this */

@define-color phosh_fg_color white; /* Font color is always white */
@define-color phosh_bg_color #00324d; /* Background color matches the wallpaper */

@define-color phosh_borders_color alpha(@phosh_fg_color, 0.1); /* Light border color */

@define-color phosh_notification_bg_color #00324d; /* Notification background */
@define-color phosh_action_bg_color #c42529; /* Action background (vibrant red) */
@define-color phosh_activity_bg_color alpha(@phosh_notification_bg_color, 0.7); /* Slightly transparent activity background */
@define-color phosh_splash_bg_color #00324d; /* Splash screen background */
@define-color phosh_splash_fg_color white; /* Splash screen text color */

/* Button colors */
@define-color phosh_button_bg_color #00324d; /* Default button background */
@define-color phosh_button_hover_bg_color shade(@phosh_button_bg_color, 1.14); /* Hover state */
@define-color phosh_button_active_bg_color shade(@phosh_button_bg_color, 1.5); /* Active state */

@define-color phosh_emergency_button_bg_color #c42529; /* Emergency button background (vibrant red) */
@define-color phosh_emergency_button_fg_color white; /* Emergency button text color */

/* Phosh-osk-stub stuff below this */

pos-input-surface {
  background-color: #00324d;
}

pos-osk-widget {
  font-family: cantarell, sans-serif;
  color: white;
  background-color: #00324d;
}

/* A regular (e.g. character, number) key */
pos-key {
  color: white;
  background: #c42529;
  border-radius: 6px;
  margin: 2px;
}

pos-key:disabled {
  color: rgba(255, 255, 255, 0.5); /* Lighter white for disabled state */
}

pos-key.pressed {
  background: #c42529;
  border: 1px solid white;
}

/* A non-character key (ESC, tab, ...) */
pos-key.sys {
  background: #c42529;
}

pos-key.sys.pressed {
  background: #c42529;
  border: 1px solid white;
}

/* a layer toggle */
pos-key.toggle {
  background: #c42529;
}

pos-key.toggle.pressed {
  background: #c42529;
  border: 1px solid white;
  color: white;
}

pos-key.return {
  background: #c42529;
  color: white;
}

pos-key.return.pressed {
  background: #c42529;
  border: 1px solid white;
  color: white;
}

/*
 * character popover
 */
pos-char-popup {
  font-family: cantarell, sans-serif;
  color: white;
  background-color: #00324d;
}

pos-char-popup grid {
  padding: 2px;
  background: #c42529;
  border-radius: 0px;
  border: 3px solid #00324d;
}

pos-char-popup button {
  font-size: 120%;
  color: white;
  background: #c42529;
  padding: 10px;
  border-radius: 0px;
  border: none;
}

pos-char-popup button:hover {
  background: #c42529;
  border: 1px solid white;
}

pos-char-popup button:active {
  background: #c42529;
  border: 1px solid white;
}

/*
 * completion bar
 */
pos-completion-bar button {
  color: white;
  background: none;
  padding-left: 10px;
  padding-right: 10px;
  border: none;
  font-weight: bold;
}

pos-completion-bar button:disabled {
  color: rgba(255, 255, 255, 0.5); /* Lighter white for disabled state */
}

pos-completion-bar button:active {
  background: #c42529;
  border: 1px solid white;
}

/*
 * menu popup
 */
#pos-menu-popup modelbutton {
  padding: 5px 10px 5px 10px;
  color: white;
  background: #00324d;
}

/*
 * shortcuts bar
 */
pos-shortcuts-bar button {
  color: white;
  background: #c42529;
  padding-left: 12px;
  padding-right: 12px;
  border: none;
  border-radius: 6px;
  margin: 2px 2px 2px 0px;
}

/* FIXME: an override to prevent double highlighting on some buttons */
pos-shortcuts-bar button:hover {
  background: #c42529;
  border: 1px solid white;
}
/* End FIXME */

pos-shortcuts-bar button:active {
  background: #c42529;
  border: 1px solid white;
}

/* Modifier button */
pos-shortcuts-bar button.toggle:checked {
  background: #c42529;
  border: 1px solid white;
}

pos-shortcuts-bar scrollbar {
  background-color: transparent;
  border: none;
  transition: none;
}

#pos-emoji-switcher {
  padding: 4px;
  background: #00324d;
}

#pos-emoji-switcher .view {
  background: transparent;
}

#pos-emoji-switcher .emoji-section {
  box-shadow: none;
}

#pos-emoji-switcher .view button {
  border-radius: 99px;
  padding: 0;
  border: none;
  text-shadow: none;
  color: white;
  background: #c42529;
}

#pos-emoji-switcher .view button:checked {
  background-color: #c42529;
  border: 1px solid white;
}

pos-emoji-picker button.action {
  color: white;
  border-radius: 6px;
  padding: 6px;
  border: none;
  box-shadow: none;
  text-shadow: none;
  background: #c42529;
}

pos-emoji-picker button.action:active {
  background: #c42529;
  border: 1px solid white;
}

pos-emoji-picker scrollbar {
  background-color: transparent;
  border: none;
  transition: none;
}

/* Keypad */
pos-keypad .digit { font-size: 120%; font-weight: bold; color: white; }
pos-keypad .letters { font-size: 60%; color: white; }
pos-keypad .symbol { font-size: 100%; color: white; }

pos-keypad button {
  color: white;
  background: #c42529;
  border: 0px;
  margin: 1px;
}

pos-keypad button:active {
  background: #c42529;
  border: 1px solid white;
}

pos-keypad button.sys {
  color: white;
  background: #c42529;
}

pos-keypad button.sys:active {
  background: #c42529;
  border: 1px solid white;
}

pos-keypad button.return {
  background: #c42529;
  color: white;
}

pos-keypad button.return:active {
  background: #c42529;
  border: 1px solid white;
  color: white;
}

/* Unset GTK defaults */
modelbutton check:checked,
modelbutton radio:active,
modelbutton radio:checked {
  background-color: #c42529;
  background-image: none;
  border-color: #c42529;
  color: white;
}

phosh-top-panel .phosh-topbar-clock {
  font-size: 10px;
}

.phosh-wwan-indicator,
phosh-top-panel .indicators {
  font-size: 10px;
}

phosh-top-panel .phosh-topbar-date {
  font-size: 10px;
}

#phosh_quick_settings flowboxchild > button.phosh-qs-active {
    font-size: 10px;
}
#phosh_quick_settings flowboxchild > button {
    font-size: 10px;
}

.phosh-audio-settings { 
font-size: 10px;
}

.phosh-settings-list-box { 
font-size: 10px;
}
.phosh-status-page {  
font-size: 10px;
}

phosh-status-page-placeholder > box > label.title {
  font-size: 10px;
}

/*
 * Notifications
 */

phosh-notification-content {
  font-size: 10px;
}

phosh-notification-content .message-area {
  font-size: 10px;
}

phosh-notification-content .actions-area {
  font-size: 10px;
}

phosh-notification-content .actions-area button {
  font-size: 10px;
}

phosh-notification-frame {
  font-size: 10px;
}

phosh-notification-frame .header-area {
  font-size: 10px;
}

phosh-notification-frame .notification-container {
  font-size: 10px;
}

phosh-notification-banner {
  font-size: 10px;
}

phosh-notification-banner > phosh-notification-frame {
  font-size: 10px;
}

.phosh-notification-tray {
  font-size: 10px;
}

.phosh-notification-tray list row {
  font-size: 10px;
}

.phosh-notifications-header {
  font-size: 10px;
}

#phosh-notifications-clear-all-btn {
  font-size: 10px;
}

phosh-call-notification {
  font-size: 10px;
}

EOF

gsettings set org.gnome.desktop.screensaver picture-uri 'file://'$HOME'/.themes/fasttheme/tuxgnu.png'
gsettings set org.gnome.desktop.interface accent-color red


notify-send Success Reboot